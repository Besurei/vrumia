/*****************************************************************************
Copyright © 2015 SDKBOX.

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*****************************************************************************/

using UnityEngine;
using UnityEditor;
using UnityEditor.Callbacks;
#if UNITY_5 && UNITY_IOS
using UnityEditor.iOS.Xcode;
#endif
using System.IO;
using Sdkbox;

namespace Sdkbox {
	[InitializeOnLoad]
	public class SocialShareEditorScript {

		const string Version = "1.0";

		[PostProcessSceneAttribute (1)]
		static SocialShareEditorScript() {
			Sdkbox.Setup.Register("SocialShare", Version);
			string lib_project = "socialshare/Assets/Plugins/Android/sdkbox_facebook_lib/";
			string folder = "Assets/SDKBOX";
			if (Directory.Exists (Path.Combine (folder, lib_project))) {
				Android.Utils.MovePlugin (lib_project);
			}
			lib_project = "socialshare/Assets/Plugins/Android/sdkbox_socialshare_lib/";
			if (Directory.Exists (Path.Combine (folder, lib_project))) {
				Android.Utils.MovePlugin (lib_project);
			}
		}

		[MenuItem("Window/SDKBOX/Documentation/SocialShare")]
		static void OpenDocumentation(MenuCommand menuCommand) {
			Sdkbox.Setup.OpenDocumentation("socialshare");
		}

		[PostProcessBuild(999)]
		public static void OnPostProcessBuild(BuildTarget target, string pathToBuiltProject) {
		    Setup.OnPostProcessBuild(target, pathToBuiltProject);
			switch (target) {
#if UNITY_IOS
#if UNITY_5
			case BuildTarget.iOS: {
#else
			case BuildTarget.iPhone: {
#endif
					#if UNITY_5
					string projPath = pathToBuiltProject + "/Unity-iPhone.xcodeproj/project.pbxproj";
					PBXProject proj = new PBXProject();
					proj.ReadFromString(File.ReadAllText(projPath));
					string targetGUID = proj.TargetGuidByName("Unity-iPhone");
					proj.AddBuildProperty(targetGUID, "OTHER_LDFLAGS", "-ObjC");
					File.WriteAllText(projPath, proj.WriteToString());
					#endif
					break;
				}
#endif
			case BuildTarget.Android:{
					var manifest = Sdkbox.Android.Manifest.Open();
					if (null != manifest) {
						manifest.OverrideUnityNativeActivity();
						manifest.Close();
					} else {
						Debug.Log("Failed to open AndroidManifest.xml");
					}
					break;
				}
			default: {
					break;
				}
			}
		}
	}
}
